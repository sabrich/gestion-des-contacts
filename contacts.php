<?php 

require 'vendor/autoload.php';
require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Charger les views (twig)
$loader = new Twig_Loader_Filesystem(__dir__. '/templates');

// Configuration de l'environnement: cache est false on utilise pas le cache
$twig = new Twig_Environment($loader, array(
    'cache' => false, //__dir__ .'/tmp',
));

// Vérifier si la session est ouverte
$auth = \App\Table\Auth::logged();
if(!$auth){
    // Redirection vers page forbidden car aucune session est ouverte
    \App\Table\Auth::forbidden();
}

// Recherche de tous les contacts de l'utilsateur connecté
$contacts = App\Table\Contact::findByUser(\App\Table\Auth::getUserId());

/*
 * Affichage de view en passant les paramètres suivants:
 * contacts: la liste des contacts (resultat de la requête)
 */
echo $twig->render('list.twig', array('contacts' => $contacts));
