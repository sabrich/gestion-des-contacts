<?php 

require 'vendor/autoload.php';
require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Charger les views (twig)
$loader = new Twig_Loader_Filesystem(__dir__. '/templates');

// Configuration de l'environnement: cache est false on utilise pas le cache
$twig = new Twig_Environment($loader, array(
    'cache' => false, //__dir__ .'/tmp',
));

// Vérifier si la session est ouverte
$auth = \App\Table\Auth::logged();
if(!$auth){
    // Redirection vers page forbidden car aucune session est ouverte
    \App\Table\Auth::forbidden();
}

// Vérifier si le formulaire n'est pas envoyé
if(!empty($_POST)){
    // insertion nouvel adresse
    $add_adresse = \App\Table\Adresse::insert( [
        'adresse'    => htmlspecialchars(stripslashes (strtoupper($_POST['adresse']))),
        'contact_id' => filter_var(($_GET['id']), FILTER_VALIDATE_INT)
    ]);
}

// Recherche de toutes les adresses du contact pour les afficher dans le sidebar
$adresses = \App\Table\Adresse::findByContact(filter_var(($_GET['id']), FILTER_VALIDATE_INT));
 
/*
 * Affichage de view en passant les paramètres suivants:
 * contact: l'id du contact
 * adresses: la liste des adresses
 * flashbag: flag pour savoir si la requête a été bien exécuté et on affiche le message de succès dans le view
 */
echo $twig->render('add_adresse.twig', array(
        'contact'  => filter_var(($_GET['id']), FILTER_VALIDATE_INT),  
        'adresses' => $adresses, 
        'flashbag' => isset($add_adresse)
    )
);
