<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

$app->post('/api/contact', function (Request $request) use ($app) {
    $data = $request->get('data');
    
    if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
        return new Response('Cet email est correct.', 200);
    } else {
        return new Response('Cet email a un format non adapté.', 400);
    }
});

$app->run();

