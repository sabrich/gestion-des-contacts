<?php 

require 'vendor/autoload.php';
require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Charger les views (twig)
$loader = new Twig_Loader_Filesystem(__dir__. '/templates');

// Configuration de l'environnement: cache est false on utilise pas le cache
$twig = new Twig_Environment($loader, array(
    'cache' => false, //__dir__ .'/tmp',
));

// Vérifier si le formulaire n'est pas envoyé
if(!empty($_POST)){
    // Requête de recherche dans la table user
    $user = App\Table\Auth::login(htmlspecialchars(stripslashes($_POST['username'])), $_POST['password']);
    
    // Redirection
    if($user){
        header('location: contacts.php');
    }else{
    ?>
        <div class="alert alert-danger">Identifiant incorrect</div>
    <?php            
    }
}
// Vérifier si la session est ouverte
$auth = \App\Table\Auth::logged();

if($auth){
    // Si l'utilisateur est connecté, on se redirige vers la la liste des contacts
    header('location: contacts.php');
}else{
    // On charge la view login
    echo $twig->render('home.twig');
}
