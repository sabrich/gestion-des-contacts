<?php 

require 'vendor/autoload.php';
require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Charger les views (twig)
$loader = new Twig_Loader_Filesystem(__dir__. '/templates');

// Configuration de l'environnement: cache est false on utilise pas le cache
$twig = new Twig_Environment($loader, array(
    'cache' => false, //__dir__ .'/tmp',
));

// Vérifier si la session est ouverte
$auth = \App\Table\Auth::logged();
if(!$auth){
    // Redirection vers page forbidden car aucune session est ouverte
    \App\Table\Auth::forbidden();
}

$message = false;

// Vérifier si le formulaire n'est pas envoyé
if(!empty($_POST)){
    // Vérifier si le nom n'est pas palindrome
    if(\App\Table\Contact::palindrome($_POST['nom'])){
        $message = true;        
    }else{
        // insertion nouveau contact
        $contact = \App\Table\Contact::insert([
            'nom'     => htmlspecialchars(stripslashes(ucfirst(strtolower($_POST['nom'])))),
            'prenom'  => htmlspecialchars(stripslashes(ucfirst(strtolower($_POST['prenom'])))),
            'email'   => htmlspecialchars(stripslashes(strtolower($_POST['email']))),
            'user_id' => \App\Table\Auth::getUserId()
        ]);
    }
}

/*
 * Affichage de view en passant les paramètres suivants:
 * flashbag: flag pour savoir si la requête a été bien exécuté ou pas
 * message: flag pour savoir si le nom est palindrome ou pas
 */
echo $twig->render('add.twig', array(
        'flashbag' => isset($contact), 
        'message'  => $message
    )
);
