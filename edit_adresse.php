<?php 

require 'vendor/autoload.php';
require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Charger les views (twig)
$loader = new Twig_Loader_Filesystem(__dir__. '/templates');

// Configuration de l'environnement: cache est false on utilise pas le cache
$twig = new Twig_Environment($loader, array(
    'cache' => false, //__dir__ .'/tmp',
));

// Vérifier si la session est ouverte
$auth = \App\Table\Auth::logged();
if(!$auth){
    // Redirection vers page forbidden car aucune session est ouverte
    \App\Table\Auth::forbidden();
}

// Vérifier si le formulaire n'est pas envoyé
if(!empty($_POST)){
    // mettre à jour une adresse
    $update_adresse = \App\Table\Adresse::update(filter_var(($_GET['id']), FILTER_VALIDATE_INT), [
        'adresse' => htmlspecialchars(stripslashes(strtoupper($_POST['adresse'])))
    ]);
}

// Recherche d'un adresse dont l'id est passé en paramétre
$adresse  = \App\Table\Adresse::findById(filter_var(($_GET['id']), FILTER_VALIDATE_INT));
// Recherche de toutes les adresses du contact pour les afficher dans le sidebar
$adresses = \App\Table\Adresse::findByContact(filter_var(($_GET['contact']), FILTER_VALIDATE_INT));
   
/*
 * Affichage de view en passant les paramètres suivants:
 * contact: l'id du contact choisi
 * adresses: la liste des adresses
 * flashbag: flag pour savoir si la requête a été bien exécuté et on affiche le message de succès dans le view
 * adresse: l'adresse passé en parametre
 */
echo $twig->render('edit_adresse.twig', array(
        'contact' => filter_var(($_GET['contact']), FILTER_VALIDATE_INT), 
        'adresse' => $adresse, 
        'adresses' => $adresses, 
        'flashbag' => isset($update_adresse)
    )
);
