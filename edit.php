<?php 

require 'vendor/autoload.php';
require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Charger les views (twig)
$loader = new Twig_Loader_Filesystem(__dir__. '/templates');

// Configuration de l'environnement: cache est false on utilise pas le cache
$twig = new Twig_Environment($loader, array(
    'cache' => false, //__dir__ .'/tmp',
));

// Vérifier si la session est ouverte
$auth = \App\Table\Auth::logged();
if(!$auth){
    // Redirection vers page forbidden car aucune session est ouverte
    \App\Table\Auth::forbidden();
}

$message = false;

// Vérifier si le formulaire n'est pas envoyé
if(!empty($_POST)){
    // Vérifier si le nom n'est pas palindrome
    if(\App\Table\Contact::palindrome($_POST['nom'])){
        $message = true;
    }else{
        // Mise à jour d'un contact
        $update_contact = \App\Table\Contact::update(filter_var(($_GET['id']), FILTER_VALIDATE_INT), [
            'nom'    => htmlspecialchars(stripslashes(ucfirst(strtolower($_POST['nom'])))),
            'prenom' => htmlspecialchars(stripslashes(ucfirst(strtolower($_POST['prenom'])))),
            'email'  => htmlspecialchars(stripslashes(strtolower($_POST['email'])))
        ]);
    }
}

// Recherche un contact
$contact  = \App\Table\Contact::findById(filter_var(($_GET['id']), FILTER_VALIDATE_INT));
// Recherche de toutes les adresses du contact pour les afficher dans le sidebar
$adresses = \App\Table\Adresse::findByContact(filter_var(($_GET['id']), FILTER_VALIDATE_INT));

/*
 * Affichage de view en passant les paramètres suivants:
 * contact: le contact choisi
 * adresses: la liste des adresses
 * flashbag: flag pour savoir si la requête a été bien exécuté et on affiche le message de succès dans le view
 * message: flag pour savoir si le nom est palindrome ou pas
 */
echo $twig->render('edit.twig', array(
        'contact'  => $contact, 
        'adresses' => $adresses, 
        'flashbag' => isset($update_contact),
        'message'  => $message
    )
);
