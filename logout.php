<?php 

require 'app/Autoloader.php';

// Enregistrer l'autoloarder
App\Autoloader::register();

// Fermer une session
$user = App\Table\Auth::logout();
