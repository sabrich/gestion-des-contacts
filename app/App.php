<?php

namespace App;

/**
 * Class App
 * Permet de la connexion à la base de données
 */
class App{
    
    /**
     * @var string Nom de base base données
     */
    const DB_NAME = 'contact';
    /**
     * @var string Nom d'utilisateur
     */
    const DB_USER = 'root';
    /**
     * @var string mot de passe
     */
    const DB_PASS = '';
    /**
     * @var string host
     */
    const DB_HOST = 'localhost';
    
    /**
     *
     * @var type Permet d'instancier et stocker la connexion à la base de données
     */
    private static $database;
    
    /**
     * @return type établir la connexion à la base de données
     */
    public static function getDb(){
        if (static::$database ===  null){
            static::$database = new Database(static::DB_NAME, static::DB_USER, static::DB_PASS, static::DB_HOST);
        }
        return static::$database;
    }
}

