<?php

namespace App\Table;

use App\App;

/**
 * Class Auth
 * 
 * Permet d'interagir avec la table "user" et qui hérite de la classe Table
 */
class Auth extends Table{
    
    /**
     * 
     * @param string $username le nom d'utilisateur 
     * @param string $password le mot de passe
     * @return boolean
     * 
     * Cherche dans la table 'user' si le compte existe et démarre une session sinon elle renvoie false
     */
    public static function login($username, $password){
        // recherche par nom d'utilisateur seulement
        $user = App::getDb()->prepare("SELECT * FROM users WHERE username = ?", [$username],null, TRUE);
       
        if($user){
            /*
             * comparer le mot de passe trouvé dans la base de données par celui entré par le utilisateur
             * Les mots de passe stockés dans la base de données sont cryptés avec l'algorithme SHA1, du coup il faut crypter le mot de passe entré par l'utilisateur         
             * si sont égaux on démarre la session
             */           
            if($user->password === sha1($password)) {
                $_SESSION['auth'] = $user->id;
               
                return true;
            }
        }        
        return false;
    }
    
    /**
     * 
     * @return integer
     * Permet de vérifier si la session 'auth' n'est pas vide
     */
    public static function logged(){
        return isset( $_SESSION['auth']);
    }
    
    /**
     * Si l'utilisateur n'est pas connecté et il essaie d'accéder à une page autre que la page login, le système se redirige vers une page Forbidden.
     */
    public static function forbidden(){
       header('HTTP/1.0 403 Forbidden');
       die('Acces interdit');
   }
   
   /**
    * 
    * @return boolean
    * @return integer
    * 
    * Permet de renvoyer l'id de l'utilisateur connecté, sinon elle renvoie false aucune session ouverte
    */
   public static function getUserId(){
       if(self::logged()){
           return $_SESSION['auth'];
       }
       
       return false;
   }
    
   /**
    * Permet de fermer une session et redirection vers la page d'authentification
    */
   public static function logout(){
       unset($_SESSION['auth']);
       header('location: index.php');
   }
}

