<?php

namespace App\Table;

use App\App;

/**
 * Class Contact
 * 
 * Permet d'intéragir avec la table "contact" et qui hérite de la classe Table
 */
class Contact extends Table{
    
    /**
     *
     * @var string Le nom de la table 
     */
    protected static $table = 'contacts';
    
    /**
     * 
     * @param integer $user l'id de l'utilisateur cnnecté
     * @return objet
     * 
     * Permet de chercher les contacts de l'utilisateur passé en paramétre
     */
    public static function findByUser($user){
        return App::getDb()->prepare(""
                . "SELECT * "
                . "FROM contacts"
                . " WHERE user_id = ?", 
                [$user],
                null, 
                false);
    }
    
    /**
     * 
     * @param string $nom la chaine à verifier
     * @return boolean
     * 
     * Permet de vérifier si le nom est palindrome ou pas
     */
    public static function palindrome($nom){        
        //supprimer tous les espaces
        $nom = str_replace(' ', '', $nom);

        //supprime les caractères spéciaux
        $nom = preg_replace('/[^A-Za-z0-9\-]/', '',$nom);

        //Changer la chaine en miniscule
        $nom = strtolower($nom);

        //inverser la chaine et stocker dans une variable
        $reverse = strrev($nom);
        
        //comparaion entre la chaine renversée et la chaine initiale
        if ($nom == $reverse) {
            return true;
        } 
        else {
            return false;
        }
    }
}

