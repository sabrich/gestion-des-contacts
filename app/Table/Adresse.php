<?php

namespace App\Table;

use App\App;

/**
 * Class Adresse
 * 
 * Permet d'intéragir avec la table "adresse" et qui hérite de la classe Table
 */
class Adresse extends Table{
    
    /**
     * 
     * @param integer $id identifiant du contact
     * @return object
     * Permet de chercher les adresses du contact passé en paramétre
     */
    public static function findByContact($id){
        return App::getDb()->prepare(""
                . "SELECT * "
                . "FROM adresses "
                . "WHERE contact_id = ?", 
                [$id],
                null, 
                false);
    }
    
}

