<?php

namespace App\Table;

use App\App;

/**
 * Classe Table
 * 
 * Classe mére, elle contient toutes les requêtes (INSERT, SELECT, UPDATE)
 */
class Table{
    
    /**
     *
     * @var string pour stocker les nom des tables 
     */
    protected static $table;
    
    /**
     * 
     * @return string
     * 
     * Permet de récupérer le nom de la table en fonction de classe chargée
     */
    private static function getTable(){
        if(static::$table == null){
            $class_name = explode('\\', get_called_class());
            static::$table = strtolower(end($class_name)). 's';
        }
        return static::$table;
    }
    
    public function __get($key) {
        $method = 'get' . ucfirst($key);
        $this->$key = $this->$method();
        return $this->$key;
    }
    
    /**
     * 
     * @return objet
     * Permet de lancer une requête SELECT * (chercher tous les enregistrements)
     */
    public static function All(){
        return App::getDb()->query(""
                . "SELECT *"
                . "FROM ".static::getTable()." "
                );
    }
    
    /**
     * 
     * @param integer $id le critére de recherche
     * @return objet
     * Permet de lancer une requête SELECT * avec la clause WHERE
     */
    public static function findById($id){
        return App::getDb()->query(""
                . "SELECT *"
                . "FROM ".static::getTable()." "
                . "WHERE id = ".$id.""
                , TRUE);
    }
    
    /**
     * 
     * @param integer $id le critére de recherche
     * @param array $fields les attributs de la requête et les valeurs
     * @return objet
     * Permet de lancer une requête UPDATE
     */
    public static function update($id, $fields){        
        $sql  = [];
        $attr = [];
        foreach($fields as $key => $field){
            $sql[] = "$key = ?";
            $attr[] = $field;
        }
        $attr[] = $id;
        $sql = implode(', ', $sql);
        
        return App::getDb()->prepare(""
                . "UPDATE ".static::getTable().""
                . " SET  $sql"
                . " WHERE id = ?"
                , $attr, true);
    }
    
    /**
     * 
     * @param array $fields les attributs de la requête et les valeurs
     * @return objet
     * Permet de lancer une requête INSERT
     */
    public static function insert($fields){        
        $sql  = [];
        $attr = [];
        $param = [];
        
        foreach($fields as $key => $field){
            
            $param[] = "?";
            $sql[] = "$key";
            $attr[] = $field;
        }
        
        $sql = implode(', ', $sql);
        $param = implode(', ', $param);
        
        return App::getDb()->prepare(""
                . "INSERT INTO ".static::getTable().""
                . " ($sql) VALUES ($param)"
                , $attr, true);
    }
}

