<?php

namespace App;

use \PDO;

/**
 * Class Database
 * Permet d'intéragir avec la base de données
 */
class Database{
    
    /**
     * @var string Nom de base base données
     */
    private $db_name;
    /**
     *
     * @var string Nom d'utilisateur
     */
    private $db_user;
    
    /**
     *
     * @var string mot de passe
     */
    private $db_pass;
    
    /**
     *
     * @var string Hôte
     */
    private $db_host;
    
    /**
     *
     * @var string Pour stocker la connexion 
     */
    private $pdo;

    /**
     * 
     * @param string $db_name
     * @param string $db_user
     * @param string $db_pass
     * @param string $db_host
     * Instanciation 
     */
    public function __construct($db_name, $db_user = 'root', $db_pass = '', $db_host  = 'localhost') {
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_host = $db_host;
    }
    
    /**
     * 
     * @return objet 
     * Permet d'établir la connexion et la stocker dans la variable $pdo
     */
    private function getPDO(){
        if ($this->pdo === NULL) {
            $pdo = new PDO('mysql:dbname=contact;host=localhost', 'root', '');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
        
            $this->pdo =  $pdo;
        }
        return $this->pdo;
    }
    
    /**
     * 
     * @param string $statement requête SQL
     * @param boolean $one permet de préciser la méthode de Fetch (fetch si $one = true, fetchall sinon)
     * @return object
     * 
     * Permet d'exécuter une requête SQL
     */
    public function query($statement, $one=false){
       
        $req = $this->getPDO()->query($statement);
        
        if($one){
            $datas = $req->fetch(PDO::FETCH_OBJ);
        }else{
            $datas = $req->fetchAll(PDO::FETCH_OBJ);
        }
        return $datas;
        
    }
    
    /**
     * 
     * @param string $statment   requête SQL paramétrée
     * @param array $attributes les paramétres à passer
     * @param string $class_name permet de préciser si chnger le mode de fetch (fetch_class si $class_nme = true, fetch_obj sinon)
     * @param boolean $one permet de préciser la méthode de Fetch (fetch si $one = true, fetchall sinon)
     * @return object
     * 
     * Permet d'exécuter une requête SQL paramétrée
     */
    public function prepare($statment, $attributes, $class_name, $one=false){

        $req = $this->getPDO()->prepare($statment);
        $res = $req->execute($attributes);
        
        $res = $req->setFetchMode(PDO::FETCH_OBJ);
        
        if( (strpos($statment, 'UPDATE') === 0) || (strpos($statment, 'INSERT') === 0))
        {
            return $res;
        }
                
        if($one){
            $datas = $req->fetch();
        }else{
            $datas = $req->fetchAll();
        }
        
        return $datas;
    }
}
