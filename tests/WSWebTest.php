<?php

namespace Silex\Tests;

use Silex\Application;
use \Silex\WebTestCase;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WSWebTest extends WebTestCase
{
    public function createApplication()
    {
        $app = new Application(); 
        
        $app->post('/api/contact', function (Request $request) use ($app) {
            $data = $request->get('data');

            if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
                return new Response('Cet email est correct.', 200);
            } else {
                return new Response('Cet email a un format non adapté.', 400);
            }
        });
        
        return $app;
    }
    
    public function testInitialPageStatusCodeIs200()
    {
        $client = $this->createClient();
        $client->request('POST', '/api/contact',
            [
                'data' => 'test@test.fr',
            ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}