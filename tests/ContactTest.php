<?php
namespace Tests;

use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    public function testCanBePalindrome(): void
    {
        $this->assertEquals(true, \App\Table\Contact::palindrome('Laval'));
    }
}


