<?php

namespace Silex\Tests;

use Silex\Application;
use PHPUnit\Framework\TestCase;

class WSWebTest extends TestCase
{
    public function testInitialPage()
    {
        $app = new Application();

        $returnValue = $app->post('/api/contact', function () {});
        $this->assertInstanceOf('Silex\Controller', $returnValue);
        
        $statusCode = 200;
        
        $this->assertEquals(200, $statusCode);
    }
}